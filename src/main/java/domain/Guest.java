package domain;

import javax.persistence.*;

@Entity
@Table(name="guests")
public class Guest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "guest_id")
    private Long guestId;

    @Column(name="name", nullable = false)
    private String name;

    public Guest(Long guestId, String name) {
        this.guestId = guestId;
        this.name = name;
    }

    public Long getGuestId() {
        return guestId;
    }

    public void setGuestId(Long guestId) {
        this.guestId = guestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
