package domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="countries")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "country_id")
    private Long countryId;

    @Column(name="name", nullable = true)
    private String name;

    @Column(name="area", nullable = false)
    private Double area;

    @Column(name="national_day", nullable = true)
    private Date nationalDay;

    @Column(name="country_code2", nullable = false)
    private String countryCode2;

    @Column(name="country_code3", nullable = false)
    private String countryCode3;

    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;


    public Country(Long countryId, String name, Double area, Date nationalDay, String countryCode2, String countryCode3,Region region ) {
        this.countryId = countryId;
        this.name = name;
        this.area = area;
        this.nationalDay = nationalDay;
        this.countryCode2 = countryCode2;
        this.countryCode3 = countryCode3;
        this.region = region;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getArea() {
        return area;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getNationalDay() {
        return nationalDay;
    }

    public void setNationalDay(Date nationalDay) {
        this.nationalDay = nationalDay;
    }

    public String getCountryCode2() {
        return countryCode2;
    }

    public void setCountryCode2(String countryCode2) {
        this.countryCode2 = countryCode2;
    }

    public String getCountryCode3() {
        return countryCode3;
    }

    public void setCountryCode3(String countryCode3) {
        this.countryCode3 = countryCode3;
    }
}
