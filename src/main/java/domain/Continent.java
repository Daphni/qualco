package domain;

import javax.persistence.*;

@Entity
@Table(name="continents")
public class Continent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "continent_id")
    private Long continentId;

    @Column(name="name", nullable = false)
    private String name;


    public Continent(Long id, String name) {
        this.continentId = id;
        this.name = name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public Long getContinentId() {
        return continentId;
    }

    public void setContinentId(Long continentId) {
        this.continentId = continentId;
    }

}
