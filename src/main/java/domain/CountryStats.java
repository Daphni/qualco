package domain;

import javax.persistence.*;

@Entity
@Table(name="country_stats")
public class CountryStats {

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @Column(name="year")
    private Integer year;

    @Column(name="population", nullable = true)
    private Integer population;

    @Column(name="area", nullable = true)
    private Double gdp;

    public CountryStats(Country country, Integer year, Integer population, Double gdp) {
        this.country = country;
        this.year = year;
        this.population = population;
        this.gdp = gdp;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Double getGdp() {
        return gdp;
    }

    public void setGdp(Double gdp) {
        this.gdp = gdp;
    }
}
