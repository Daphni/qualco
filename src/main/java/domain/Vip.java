package domain;

import javax.persistence.*;

@Entity
@Table(name="vips")
public class Vip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "vip_id")
    private Long vipId;

    @Column(name="name", nullable = false)
    private String name;

    public Vip(Long vipId, String name) {
        this.vipId = vipId;
        this.name = name;
    }

    public Long getVipId() {
        return vipId;
    }

    public void setVipId(Long vipId) {
        this.vipId = vipId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
