package domain;

import javax.persistence.*;

@Entity
@Table(name="languages")
public class Language {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "language_id")
    private Long languageId;

    @Column(name="language", nullable = false)
    private String  language;

    public Language(Long languageId, String language) {
        this.languageId = languageId;
        this.language = language;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
