package domain;

import javax.persistence.*;

@Entity
@Table(name="country_languages", uniqueConstraints = {@UniqueConstraint(columnNames = {"country_id","language_id"})})
public class CountryLanguage {

        @ManyToOne
        @JoinColumn(name = "country_id")
        private Country country;

        @ManyToOne
        @JoinColumn(name = "language_id")
        private Language language;

        @Column(name="official",  nullable = false)
        private Integer official;

        public CountryLanguage(Country country, Language language, Integer official) {
                this.country = country;
                this.language = language;
                this.official = official;
        }

        public Country getCountry() {
                return country;
        }

        public void setCountry(Country country) {
                this.country = country;
        }

        public Language getLanguage() {
                return language;
        }

        public void setLanguage(Language language) {
                this.language = language;
        }

        public Integer getOfficial() {
                return official;
        }

        public void setOfficial(Integer official) {
                this.official = official;
        }
}
