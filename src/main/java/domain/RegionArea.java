package domain;

import javax.persistence.*;

@Entity
@Table(name="region_areas")
public class RegionArea {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "region_name")
    private String region_name;

    @Column(name="region_area", nullable = false)
    private Double regionArea;

    public RegionArea(String region_name, Double regionArea) {
        this.region_name = region_name;
        this.regionArea = regionArea;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public Double getRegionArea() {
        return regionArea;
    }

    public void setRegionArea(Double regionArea) {
        this.regionArea = regionArea;
    }
}
