package Converter;


import domain.CountryLanguage;
import dto.CountryLanguageDto;

import static Converter.CountryConverter.convertCountry;
import static Converter.LanguageConverter.convertLanguage;


public class CountryLanguageConverter {

    public static CountryLanguageDto convertCountryLanguage(CountryLanguage countryLanguage){
        CountryLanguageDto countryLanguageDto = new CountryLanguageDto();
        countryLanguageDto.setCountry(convertCountry(countryLanguage.getCountry()));
        countryLanguageDto.setLanguage(convertLanguage(countryLanguage.getLanguage()));
        return countryLanguageDto;
    }

}
