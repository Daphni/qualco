package Converter;


import domain.Country;
import dto.CountryDto;

public class CountryConverter {

    public static CountryDto convertCountry(Country country){
        CountryDto countryDtoDTO = new CountryDto();
        countryDtoDTO.setCountryId(country.getCountryId());
        countryDtoDTO.setName(country.getName());
        countryDtoDTO.setArea(country.getArea());
        countryDtoDTO.setCountryCode2(country.getCountryCode2());
        return countryDtoDTO;
    }

}
