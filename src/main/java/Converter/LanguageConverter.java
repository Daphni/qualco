package Converter;


import domain.Language;
import dto.LanguageDto;

public class LanguageConverter {
    public static LanguageDto convertLanguage(Language language){
        LanguageDto languageDto = new LanguageDto();
        languageDto.setLanguageId(language.getLanguageId());
        languageDto.setLanguage(language.getLanguage());
        return languageDto;
    }
}

