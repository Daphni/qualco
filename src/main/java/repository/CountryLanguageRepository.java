package repository;


import domain.CountryLanguage;

import java.util.List;


public interface CountryLanguageRepository {

    List<CountryLanguage> findAll();

    List<CountryLanguage> findByCountry(Long id);

}
