package repository;


import domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CountryRepository extends JpaRepository<Country,Long> {

    List<Country> findAll();

    Optional<Country> findById(Long id);

}
