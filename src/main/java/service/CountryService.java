package service;

import dto.CountryDto;

import java.util.List;
import java.util.Optional;

public interface CountryService {

        Optional<CountryDto> findAllCountries(Long id);

        List<CountryDto> getAllCountries();

}
