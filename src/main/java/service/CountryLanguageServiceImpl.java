package service;


import Converter.CountryLanguageConverter;
import dto.CountryLanguageDto;
import org.springframework.beans.factory.annotation.Autowired;
import repository.CountryLanguageRepository;

import java.util.Optional;

public class CountryLanguageServiceImpl implements CountryLanguageService {

    @Autowired
    private CountryLanguageRepository countryLanguageRepository;


    @Override
    public Optional<CountryLanguageDto> findLanguageByCountryId(Long id) {
        return countryLanguageRepository.findByCountry(id).stream()
                .map(CountryLanguageConverter::convertCountryLanguage)
                .findFirst();
    }

}
