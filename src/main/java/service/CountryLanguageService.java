package service;


import dto.CountryLanguageDto;

import java.util.Optional;


public interface CountryLanguageService {
    Optional<CountryLanguageDto> findLanguageByCountryId(Long id);
}
