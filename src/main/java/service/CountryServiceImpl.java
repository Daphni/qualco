package service;


import Converter.CountryConverter;
import dto.CountryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.CountryRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl  implements CountryService{

    @Autowired
    private CountryRepository countryRepository;


    @Override
    public Optional<CountryDto> findAllCountries(Long id) {
        return countryRepository.findAllById(Collections.singleton(id)).stream()
                .map(CountryConverter::convertCountry)
                .findFirst();
    }

    @Override
    public List<CountryDto> getAllCountries() {
        return countryRepository.findAll().stream()
                .map(CountryConverter::convertCountry)
                .collect(Collectors.toList());
    }

}

